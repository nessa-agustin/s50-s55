import { useEffect, useState, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    let regInit = {
        firstName: '',
        lastName: '',
        mobileNo: '',
        email: '',
        password: '',
        password2: '',
    }

    const [regUser, setRegUser] = useState(regInit);
    const [isActive, setIsActive] = useState(false);

    function registerUser(e){
        e.preventDefault();

        const urlPath = process.env.REACT_APP_API_URL;

        fetch(`${urlPath}/users/checkEmail`,{
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({email: regUser.email})
        }).then(res => res.json())
        .then(data => {
            console.log(data)

            if(data)//email exists
            {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                })
            }else{ // register User
                let urlReg = `${urlPath}/users/register`;
                fetch(urlReg, {
                    method: 'POST',
                    headers: {
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify(regUser)
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                        setRegUser(regInit);
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            "text": "Welcome to Zuitt!"
                        })

                        navigate('/login')
                })

            }

        })


            

    }

    useEffect(() => {

        if((regUser.firstName !== '' && regUser.lastName !== '' && regUser.email !== '' && regUser.mobileNo.length >= 11) && (regUser.password === regUser.password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    },[regUser])


    function getChanges(field){
        return (e) => setRegUser((regUser) => ({...regUser,[field]:e.target.value}));
    }


    return (

        (user.id !== null) ? //user is logged in
            <Navigate to='/courses' />
            :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group className="mb-3" controlId="firstname">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type="text"
                    value={regUser.firstName}
                    onChange={getChanges('firstName')}
                    placeholder="First Email" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type="text"
                    value={regUser.lastName}
                    onChange={getChanges('lastName')}
                    placeholder="Last Name" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    value={regUser.email}
                    onChange={getChanges('email')}
                    placeholder="johndoe@email.com" />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile  Number</Form.Label>
                <Form.Control
                    type="text"
                    value={regUser.mobileNo}
                    onChange={getChanges('mobileNo')}
                    placeholder="+63-" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    value={regUser.password}
                    onChange={getChanges('password')}
                    placeholder="Enter Password" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                    type="password"
                    value={regUser.password2}
                    onChange={getChanges('password2')}
                    placeholder="Confirm Password" />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id='submitBtn'>
                    Submit
                </Button>
                :
                <Button variant="primary" type="submit" id='submitBtn' disabled>
                    Submit
                </Button>

            }
        </Form>
    )



}