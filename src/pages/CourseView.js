import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function CourseView() {

    const {user} = useContext(UserContext)

    // allows us to gain access to methods that will allows us to redirect a user to a different page after enrolling in a course

    const navigate = useNavigate(); //previously useHistory

    const {courseId} = useParams();

    let courseInit = {
        name: '',
        description: '',
        price: 0
    }
    const [course, setCourse] = useState(courseInit);

    const enroll = (courseId) => {
        let url = `${process.env.REACT_APP_API_URL}/users/enroll`

        fetch(url,{
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                 Swal.fire({
                    title: 'Successfully enrolled',
                    icon: 'success',
                    text: 'You have successfully enrolled for this course'
                })

                navigate('/courses')

            }else{
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again'
                })
            }
        })
    }

    useEffect(() => {
        console.log(courseId)
        let url = `${process.env.REACT_APP_API_URL}/courses/${courseId}`;
        fetch(url)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setCourse(data)
            })

    },[courseId])

    return (

        <Container>
            <Row className='my-3'>
                <Col xs={12} md={6}>
                    <Card >
                        <Card.Body>
                            <Card.Title>{course.name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{course.description}</Card.Text>
                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>M-F 530PM - 930PM</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{course.price}</Card.Text>
                            
                            {
                                (user.id !== null) ? 
                                    <Button className='bg-primary' onClick={() => enroll(courseId)}>Enroll</Button>
                                :
                                    <Button className='bg-primary' as={Link} to='/login'>Login To Enroll</Button>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>




    )





}