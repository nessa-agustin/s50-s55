import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses() {


	const [courses, setCourses] = useState([])

	useEffect(() => {
		const url = `${process.env.REACT_APP_API_URL}/courses/`;
		fetch(url)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setCourses(data.map(course => {
					return (
						<CourseCard  key={course._id} courseProp={course} />
					)
				}))

			})

	},[])

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard  key={course.id} courseProp={course} />
	// 	)
	// })

	return (
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>

	)
}