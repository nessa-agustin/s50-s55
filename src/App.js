// react components/ dependencies
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

// components
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
// pages
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage'
import './App.css';

import {UserProvider} from './UserContext'

function App() {

  const urlPath = process.env.REACT_APP_API_URL;

  // State hook for the user state that is defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user,setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

  // console.log(process.env.REACT_APP_API_URL)

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    let url = `${urlPath}/users/details`;
    fetch(url, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(typeof data._id !== 'undefined'){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }else{
          setUser({
            id: null,
            isAdmin: null
          })
        }


    })


  },[])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
            <Routes>
              <Route path='/' element={<Home />}/>
              <Route path='/courses' element={<Courses />}/>
              <Route path='/courses/:courseId' element={<CourseView />}/>
              <Route path='/register' element={<Register />}/>
              <Route path='/login' element={<Login />}/>
              <Route path='/logout' element={<Logout />}/>
              <Route path='*' element={<ErrorPage />}/>
            </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
