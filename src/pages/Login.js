import { useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

    const urlPath = process.env.REACT_APP_API_URL

    // This allows us to consume the UserContext object and its properties to be used for validation
    const {user, setUser} = useContext(UserContext);

    const [isActive, setIsActive] = useState(false)

    let credInit = {email: '', password: ''};
    const [cred, setCred] = useState(credInit)

    function getChanges(field){
        return (e) => setCred((cred) => ({...cred,[field]:e.target.value}));
    }

    useEffect(() => {
        if(cred.email !== '' && cred.password !== ''){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
        
    },[cred,isActive])

    function login(e){
        e.preventDefault();
        let url = `${urlPath}/users/login`
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(cred)
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== 'undefined'){
                localStorage.setItem('token',data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    "text": "Welcome to Zuitt!"
                })
            }else{
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please check your login details then try again."
                })
            }

        })


        // localStorage.setItem('email',cred.email)
        // setUser({email: localStorage.getItem('email')})
        setCred(credInit)

        // console.log(`${cred.email} has been verified! Welcome back!`)
        // alert('You are now logged in')
    }

    const retrieveUserDetails = (token) => {
        let url = `${urlPath}/users/details`;

        fetch(url, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    return (

        (user.id !== null) ? 
            <Navigate to='/courses' />
            :
            <Form onSubmit={(e) => login(e)}>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        value={cred.email} 
                        onChange={getChanges('email')}
                        placeholder="johndoe@email.com" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        value={cred.password} 
                        onChange={getChanges('password')}
                        placeholder="******" />
                </Form.Group>
                {/* <Button variant="primary" type="submit" id='submitBtn'>
                    Submit
                </Button> */}
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
		        }
            </Form>
        

    )
}