import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

  export default function CourseCard({courseProp}) {
          // Checks to see if the data was succesffully passed
          // console.log(props);
          //  Every component receives information in a form of an object
          // console.log(typeof props);
        //   console.log({courseProp});

          const { name, description, price, _id } = courseProp;
        
          /* 
            Use the state hook for this component to be abe to store its state. States are used to keep track of the information related to individual components/elements.

            Syntax: 
                const [getter, setter] = useState(initialGetterValue);
          
          */

        // stateHook to store the state of enrollees
        // const [count, setCount] = useState(0)
        // const [seats, setSeats] = useState(10)

     /*    function enroll(){
            if(seats === 0){
                alert('No more seats')
            }else{
                setCount(count + 1)
                setSeats(seats - 1)
            }
            console.log(`Enrollees: ${count} / Seats: ${seats}`)

           
        } */

        // function enroll(){
        //     if (seats > 0 ) {
        //       setCount(count + 1)
        //       console.log('Enrollees: ' + count)
        //       setSeats(seats - 1)
        //       console.log('Seats: ' + seats)
        //     } 
        //     // else {
        //     //   alert("No more seats available.")
        //     // }
        //   };


          // useEffect(() => {
          //   if(seats === 0){
          //       alert('No more seats available')
          //   }
          // },[seats])


          return (
              <Row className='my-3'>
                  <Col xs={12} md={6}>
                      <Card >
                          <Card.Body>
                              <Card.Title>{name}</Card.Title>
                              <Card.Subtitle>Description:</Card.Subtitle>
                              <Card.Text>{description}</Card.Text>
                              <Card.Subtitle>Price:</Card.Subtitle>
                              <Card.Text>{price}</Card.Text>
                              <Button className='bg-primary' as={Link} to={`/courses/${_id}`} >Details</Button>
                          </Card.Body>
                      </Card>
                  </Col>
              </Row>
          )
      }
